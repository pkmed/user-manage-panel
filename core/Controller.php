<?php

/**
 * Description of Controller
 *
 * @author skiro
 */

require_once 'DBHandler.php';
require_once 'PageBuilder.php';

class Controller {

    //constant for getUsersList()
    public const GET_ALL_USERS = 0;

    //constants for openAddEditForm()
    public const ADD_MODE = 10;
    public const EDIT_MODE = 11;
    public const REGISTER_MODE = 12;

	//error constants
    private const LOGIN_ERROR_NOT_EXIST_WRONG_PASS = [1101,'user doesn\'t exists or wrong password'];
    private const EDIT_ERROR_USER_NOT_EXIST = [1001,'user doesn\'t exists'];
    private const PAGE_NOT_EXIST = [404,'page doesn\'t exist'];

    //for password hashing
    private const HASH_ROUNDS = 2**5;
    private const HASH_ALGO_PASS='sha512';
    private const HASH_ALGO_SALT='sha256';

    public $db;
    public $pageBuilder;

    public function __construct(){
        $this->db = new DBHandler();
        $this->pageBuilder = new PageBuilder();
    }
    /**
     * <p>Checks login data and compare hash from table `users` and hash of entered password</p>
     * @param string $login
     * @param string $passwd
     * @return bool
     */
    public function loginUser(string $login, string $passwd){
        $userData = $this->getUserByLogin($login);
        $passwd = $this->genPasswdHash($passwd,$this->genSalt($login,$userData['name']));
        if(!array_key_exists('error',$userData) && $userData['passwd']==$passwd ){
            $_SESSION['currentUser']=$_POST['login'];
            if($this->checkAdminStatus($userData['user_id'])){
                $_SESSION['adminStatus'] = true;
            }
            return true;
        } else {
            $_SESSION['messageCode'] = self::LOGIN_ERROR_NOT_EXIST_WRONG_PASS[0];
            $_SESSION['messageText'] = self::LOGIN_ERROR_NOT_EXIST_WRONG_PASS[1];
            return false;
        }
    }
    /**
     * <p>Checks corresponding row from table `admins`</p>
     * @param int $userId
     * @return bool
     */
    public function checkAdminStatus(int $userId){
        if(!is_array($user = $this->db->checkAdminStatus($userId))){
            return false;
        } else {
            return true;
        }
    }
    /**
     * <p>Assembles user list page</p>
     * @param int $pageNumber<p>Necessary page to view</p>
     * @param int $amountOnPage<p>Amount of entries</p>
     * @param string $orderBy<p>Table sorting field</p>
     * @param string $sortOrder<p>Ascending or descending</p>
     * @return string<p>Assembled page code as string</p>
     */
    public function showUsersList(int $pageNumber=1, int $amountOnPage=Config::DEFAULT_AMOUNT_ON_PAGE, string $orderBy=Config::DEFAULT_SORT_BY, string $sortOrder=Config::DEFAULT_SORT_ORDER){
        //page counting
        $rowsTotal = count($this->getUsersList(self::GET_ALL_USERS));
        $totalPageNumber = (int)($rowsTotal/$amountOnPage);
        $totalPageNumber = $rowsTotal % $amountOnPage ? ++$totalPageNumber : $totalPageNumber;

        //page number for links
        if($pageNumber>=1 && $pageNumber<=$totalPageNumber){
            $rowsData = $this->getUsersList($amountOnPage,$pageNumber,$orderBy,$sortOrder);
            $previousPageNumber = $pageNumber==1 ? $pageNumber : $pageNumber - 1;
            $nextPageNumber = $pageNumber==$totalPageNumber ? $pageNumber : $pageNumber + 1;
        } else {
            $_SESSION['messageCode']=self::PAGE_NOT_EXIST[0];
            $_SESSION['messageText']=self::PAGE_NOT_EXIST[1];
            $rowsData = $this->getUsersList($amountOnPage,1,$orderBy,$sortOrder);
            $previousPageNumber = $nextPageNumber = 1;
        }

        //part-templates filling
        foreach ($rowsData as $rowData){
            $rowsTpls[] = implode("",$this->pageBuilder->tplDataFiller($rowData,$this->pageBuilder->templates['row']));
        }
        for($i=1; $i<=$totalPageNumber; $i++){
            $pageLinks[] = implode("",$this->pageBuilder->tplDataFiller(['pageNumber'=>$i],$this->pageBuilder->templates['pageNumber']));
        }
        $previousPageLinkTpl = $this->pageBuilder->tplDataFiller(['previousPageNumber'=>$previousPageNumber],$this->pageBuilder->templates['previousPageLink']);
        $nextPageLinkTpl = $this->pageBuilder->tplDataFiller(['nextPageNumber'=>$nextPageNumber],$this->pageBuilder->templates['nextPageLink']);

        //assembling parts and base
        $parts = [
            'userRows'=>implode("",$rowsTpls),
            'previousPageLink'=>implode("",$previousPageLinkTpl),
            'pageNumbers'=>implode("",$pageLinks),
            'nextPageLink'=>implode("",$nextPageLinkTpl)
        ];
        $usersListTpl = $this->pageBuilder->tplAssembler($this->pageBuilder->templates['usersList'],$parts);

        //insert nickname of current user in toolbar field
        $usersListTpl = $this->pageBuilder->tplDataFiller($_SESSION,$usersListTpl);
        return implode("",$usersListTpl);
    }
    /**
     * <p>Shows idle page</p>
     * @return string<p>Assembled page code as string</p>
     */
    public function showIdlePage(){
        $idlePage = $this->pageBuilder->templates['idlePage'];
        if($_SESSION['adminStatus']){
            $link = ['toInterfacePageLink'=>implode("",file("templates/toInterfacePageLink.html"))];
        } else {
            $link = [];
        }
        $idlePage = implode("",$this->pageBuilder->tplAssembler($idlePage,$link));
        return $idlePage;
    }
    /**
     * Founds needed user entry
     * @param string $login
     * @return string<p>Assembled page code as string</p>
     */
    public function showFoundUser(string $login){
        $rowsData = $this->getUserByLogin($login);

        if(array_key_exists('error',$rowsData)){
            $_SESSION['messageCode']='error';
            $_SESSION['messageText']=$rowsData['error'];
            return $this->showUsersList();
        } else {
            $rowsTpls[] = implode("",$this->pageBuilder->tplDataFiller($rowsData,$this->pageBuilder->templates['row']));
            $rowsTpls[] = implode("",$this->pageBuilder->templates['discardSearchRow']);

            $previousPageLinkTpl = $this->pageBuilder->tplDataFiller(['previousPageNumber'=>1],$this->pageBuilder->templates['previousPageLink']);
            $nextPageLinkTpl = $this->pageBuilder->tplDataFiller(['nextPageNumber'=>1],$this->pageBuilder->templates['nextPageLink']);
            $usersListTpl = $this->pageBuilder->tplDataFiller($_SESSION,$this->pageBuilder->templates['usersList']);

            $parts = [
                'userRows'=>implode("",$rowsTpls),
                'previousPageLink'=>implode("",$previousPageLinkTpl),
                'pageNumbers'=>implode("",[1]),
                'nextPageLink'=>implode("",$nextPageLinkTpl)
            ];
            $usersListTpl = $this->pageBuilder->tplAssembler($usersListTpl,$parts);

            return implode("",$usersListTpl);
        }
    }
    /**
     * <p>Assembles message for viewing messages or errors</p>
     * @param array $messageData
     * @return string
     */
    public function showMessageWindow(array $messageData){
        $window = implode("",$this->pageBuilder->tplDataFiller($messageData,$this->pageBuilder->templates['messageWindow']));
        return $window;
    }
    /**
     * <p>Assembles form for adding, editing user through interface or registration</p>
     * @param int $formMode
     * @return string
     */
    public function showAddEditForm(int $formMode){
        $pageToReturn = isset($_GET['pageNumber']) ? $_GET['pageNumber'] : 1;
        switch($formMode){
            case self::ADD_MODE:
                $backBtn = $this->pageBuilder->tplDataFiller(['pageNumber'=>$pageToReturn],$this->pageBuilder->templates['backBtn']);
                $parts=[
                    'addUserBtn'=>implode("",$this->pageBuilder->templates['addUserBtn']),
                    'backBtn'=>implode($backBtn)
                ];
                $addForm = $this->pageBuilder->tplAssembler($this->pageBuilder->templates['addEditForm'],$parts);
                $addForm = $this->pageBuilder->tplDataFiller(['pageNumber'=>$pageToReturn],$addForm);
                return implode("",$addForm);
            case self::EDIT_MODE:
                $backBtn = $this->pageBuilder->tplDataFiller(['pageNumber'=>$pageToReturn],$this->pageBuilder->templates['backBtn']);
                $parts=[
                    'editFormBtns'=>implode($this->pageBuilder->templates['editFormBtns']),
                    'backBtn'=>implode($backBtn)
                ];
                $userData = $this->getUserCard($_POST['user_id']);
                $editForm = $this->pageBuilder->tplAssembler($this->pageBuilder->templates['addEditForm'],$parts);
                $editForm = $this->pageBuilder->tplDataFiller($userData,$editForm);
                return implode("",$editForm);
            case self::REGISTER_MODE:
                $backBtn = $this->pageBuilder->tplDataFiller(['pageNumber'=>''],$this->pageBuilder->templates['backBtn']);
                $parts=[
                    'registerBtn'=>implode($this->pageBuilder->templates['registerBtn']),
                    'backBtn'=>implode($backBtn)
                ];
                $registerForm = $this->pageBuilder->tplDataFiller(['pageNumber'=>''],$this->pageBuilder->templates['addEditForm']);
                $registerForm = $this->pageBuilder->tplAssembler($registerForm,$parts);
                return implode("",$registerForm);
        }
    }
    /**
     * <p>Gets user by login or returns error message for inner check</p>
     * @param string $login
     * @return array|null
     */
    public function getUserByLogin(string $login){
        if(!is_array($user = $this->db->getUserByLogin($login))){
            $output = ['error'=>'user does not exist'];
        } else {
            $output = $user;
        }
        return $output;
    }
    /**
     * <p>Gets user entries from table `users` and selects the needed for view part</p>
     * @param int $amountOnPage
     * @param int $pageNumber
     * @param string $orderBy
     * @param string $sortOrder
     * @return array|bool
     */
    public function getUsersList(int $amountOnPage=Config::DEFAULT_AMOUNT_ON_PAGE, int $pageNumber=1, string $orderBy=Config::DEFAULT_SORT_BY, string $sortOrder=Config::DEFAULT_SORT_ORDER){
        $rows = $this->db->getUsersList($orderBy,$sortOrder);
        if(is_bool($rows)){
            $output = ['error'=>'inappropriate parameter type'];
        } else {
            if($amountOnPage != self::GET_ALL_USERS){
                $lowerBound=$amountOnPage*$pageNumber-$amountOnPage;
                $output = array_slice($rows,$lowerBound,$amountOnPage);
            } else {
                $output = $rows;
            }
        }
        return $output;
    }
    /**
     * <p>Gets user data by id or returns error message for inner check</p>
     * @param int $userId
     * @return array|null
     */
    public function getUserCard(int $userId){
        if(!is_array($user = $this->db->getUserCard($userId))){
            $output = ['error'=>'user does not exist'];
        } else {
            $output = $user;
        }
        return $output;
    }
    /**
     * <p>Generates password hash and calls method for adding new user entry</p>
     * @param string $login
     * @param string $passwd
     * @param string $name
     * @param string $surname
     * @param string $gender
     * @param string $birthDate
     * @return bool <p>Returns true if succeeded and error message in other case</p>
     */
    public function addUser(string $login, string $passwd, string $name, string $surname, string $gender, string $birthDate){
        $passwd = $this->genPasswdHash($passwd,$this->genSalt($login,$name));
        $result = $this->db->addUser($login,$passwd,$name,$surname,$gender,$birthDate);
        if(!array_search('0:',$result)){
            $_SESSION['messageCode'] = explode(':',$result['result_code'])[0];
            $_SESSION['messageText'] = explode(':',$result['result_code'])[1];
            return false;
        } else {
            return true;
        }
    }
    /**
     * <p>Generates sha512 hash of password within HASH_ROUNDS rounds</p>
     * @param string $passwd
     * @param string $salt
     * @return string
     */
    private function genPasswdHash(string $passwd, string $salt){
        $roundResult=hash(self::HASH_ALGO_PASS,$salt.$passwd);
        for ($i=0;$i<self::HASH_ROUNDS;$i++){
            $roundResult=hash(self::HASH_ALGO_PASS,$salt.$roundResult);
        }
        return $roundResult;
    }
    /**
     * <p>Generates sha256 hash for password hash generate</p>
     * @param string $login
     * @param string $name
     * @return string
     */
    private function genSalt(string $login, string $name){
        return hash(self::HASH_ALGO_SALT,$login.$name);
    }
    /**
     * @param int $userId
     * @return bool
     */
    private function isUserExists(int $userId) : bool{
        return array_key_exists('user_id',$this->getUserCard($userId));
    }
    /**
     * <p>Edits existing user data and fills error message on failure</p>
     * @param int $userId
     * @param string $login
     * @param string $passwd
     * @param string $name
     * @param string $surname
     * @param string $gender
     * @param string $birthDate
     */
    public function editUser(int $userId, string $login, string $passwd, string $name, string $surname, string $gender, string $birthDate){
        if($this->isUserExists($userId)) {
            $originalUserData = $this->getUserCard($userId);
            if($originalUserData['passwd'] != $passwd || $originalUserData['login'] != $login || $originalUserData['name'] != $name){
                $passwd = $this->genPasswdHash($passwd,$this->genSalt($login,$name));
            }
            $result = $this->db->editUser($userId, $login, $passwd, $name, $surname, $gender, $birthDate);
            if (!array_search('0:', $result)) {
                $_SESSION['messageCode'] = explode(':', $result['result_code'])[0];
                $_SESSION['messageText'] = explode(':', $result['result_code'])[1];
            }
        } else {
            $_SESSION['messageCode'] = self::EDIT_ERROR_USER_NOT_EXIST[0];
            $_SESSION['messageText'] = self::EDIT_ERROR_USER_NOT_EXIST[1];
        }
    }
    /**
     * <p>Removes user by id</p>
     * @param int $userId
     * @return array|string|null <p>On failure returns error message for inner check from getUserCard(), else returns true on success or mysql error code</p>
     */
    public function removeUser(int $userId){
        if($this->isUserExists($userId)) {
            $result = $this->db->removeUser($userId);
            if(array_search('0:',$result)){
                return true;
            } else {
                return $result;
            }
        } else {
            return $this->getUserCard($userId);
        }
    }
}