<?php

/**
 * Description of Config
 *
 * @author skiro
 */

class Config {
    public static $dbHost='localhost';
    public static $dbName='testjob';
    public static $dbUser='dummyAdmin';
    public static $dbPasswd='12345';

    public const DEFAULT_AMOUNT_ON_PAGE = 10;
    public const DEFAULT_SORT_ORDER='asc';
    public const DEFAULT_SORT_BY='login';

    public static $dataReplacementTags=[
        'user_id'=>'*id*',
        'login'=>'*userLogin*',
        'passwd'=>'*userPasswd*',
        'name'=>'*userName*',
        'surname'=>'*userSurname*',
        'gender'=>'*userGender*',
        'birth_date'=>'*userBirthDate*',
        'pageNumber'=>'*pageNumber*',
        'nextPageNumber'=>'*nextPageNumber*',
        'previousPageNumber'=>'*previousPageNumber*',
        'currentUser'=>'*curUser*',
        'messageCode'=>'*messageCode*',
        'messageText'=>'*messageText*'
    ];
    public static $tplReplacementTags=[
        'userRows'=>'*rows*',
        'previousPageLink'=>'*previousPageLink*',
        'pageNumbers'=>'*pageNumbers*',
        'nextPageLink'=>'*nextPageLink*',
        'addUserBtn'=>'*addUserBtn*',
        'editFormBtns'=>'*editFormBtns*',
        'registerBtn'=>'*registerBtn*',
        'backBtn'=>'*backBtn*',
        'toInterfacePageLink'=>'*toInterfacePageLink*'
    ];
}
