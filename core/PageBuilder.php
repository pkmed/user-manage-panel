<?php

class PageBuilder
{
    public $templates;

    public function __construct()
    {
        $this->templates = [
            'row'=>file("templates/userRow.html"),
            'discardSearchRow'=>file("templates/discardSearch.html"),
            'previousPageLink'=>$previousPageLinkTpl = file("templates/previousPageLink.html"),
            'nextPageLink'=>file("templates/nextPageLink.html"),
            'pageNumber'=>file("templates/pageNumber.html"),
            'usersList'=>file("templates/usersList.html"),
            'addEditForm'=>file("templates/addEditForm.html"),
            'addUserBtn'=>file("templates/addUserBtn.html"),
            'toInterfacePage'=>file("templates/toInterfacePageLink.html"),
            'idlePage'=>file("templates/idlePage.html"),
            'editFormBtns'=>file("templates/editFormBtns.html"),
            'messageWindow'=>file("templates/messageWindow.html"),
            'registerBtn'=>file("templates/registerBtn.html"),
            'backBtn'=>file("templates/backBtn.html")
        ];
    }

    /**
     * Reads template by lines then replace any data replacement tag witch exists in both, template line and data array.
     * If data replacement tag doesn't exists in data array it will be replaced by empty string.
     * In case if string doesn't contain any data replacement tag it will be placed in result array.
     * @param array $data<p>
     *      Associative array where keys are names of columns of table `users` or data on table data based
     * </p>
     * @param array $tpl<p>
     *      Template where needs to insert data
     * </p>
     * @return array $result<p>
     *      Template filled with data
     * </p>
     */
    public function tplDataFiller(array $data, array $tpl) : array{
        foreach ($tpl as $tplString) {
            $isAdded=false;
            foreach (Config::$dataReplacementTags as $dataReplacementTag) {
                if (strpos($tplString, $dataReplacementTag) && array_key_exists(array_search($dataReplacementTag,Config::$dataReplacementTags), $data)) {
                    $result[] = str_replace($dataReplacementTag, $data[array_search($dataReplacementTag,Config::$dataReplacementTags)], $tplString);
                    $isAdded=true;
                } else if(strpos($tplString, $dataReplacementTag)) {
                    $result[] = str_replace($dataReplacementTag, "", $tplString);
                    $isAdded=true;
                }
            }
            if(!$isAdded)
                $result[] = $tplString;
        }
        return $result;
    }

    /**
     * Reads base template by lines and replace any found tag with associated part-template from array of parts.
     * If template replacement tag doesn't exists in parts array it will be replaced by empty string.
     * In case if string doesn't contain any template replacement tag it will be placed in result array.
     * @param array $baseTpl<p>
     *      Base template for assembly
     * </p>
     * @param array $parts<p>
     *      Array of part-templates as strings
     * </p>
     * @return array $result<p>
     *      Assembled template
     * </p>
     */
    public function tplAssembler(array $baseTpl, array $parts){
        foreach($baseTpl as $tplString){
            $isAdded=false;
            foreach(Config::$tplReplacementTags as $tplReplacementTag){
                if(strpos($tplString,$tplReplacementTag) && array_key_exists(array_search($tplReplacementTag,Config::$tplReplacementTags), $parts)){
                    $result[] = str_replace($tplReplacementTag, $parts[array_search($tplReplacementTag,Config::$tplReplacementTags)], $tplString);
                    $isAdded=true;
                } else if(strpos($tplString, $tplReplacementTag)) {
                    $result[] = str_replace($tplReplacementTag, "", $tplString);
                    $isAdded=true;
                }
            }
            if(!$isAdded)
                $result[] = $tplString;
        }
        return $result;
    }
}