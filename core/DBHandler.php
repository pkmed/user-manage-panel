<?php

/**
 * Description of DBHandler
 *
 * @author skiro
 */

require_once 'Config.php';

class DBHandler {

    /**
     * Gets all users
     * @param string $orderBy<p>
     *      `Users` field by which result ordered
     * </p>
     * @param string $sortOrder<p>
     *      Ascending or descending order
     * </p>
     * @return array|bool<p>
     *      If query is successful returns strings as associative arrays nested in resulting array,
     *      otherwise return false
     * </p>
     */
    public function getUsersList(string $orderBy, string $sortOrder){
        $conn = new mysqli(Config::$dbHost,Config::$dbUser,Config::$dbPasswd, Config::$dbName);
        $result = $conn->query("select * from `users` order by `{$orderBy}` {$sortOrder}");
        $conn->close();
        if ($result instanceof mysqli_result) {
            while($row = $result->fetch_assoc()){
                $rows[] = $row;
            }
            return $rows;
        } else {
            return $result;
        }
    }
    /**
     * @param string $login<p>
     *      Login of searched user
     * </p>
     * @return array|null<p>
     *      Returns associative array with user data on successful, false otherwise
     * </p>
     */
    public function getUserByLogin(string $login){
        $conn = new mysqli(Config::$dbHost,Config::$dbUser,Config::$dbPasswd, Config::$dbName);
        $prepare = $conn->prepare("select * from `users` where `login`=?");
        $prepare->bind_param('s',$login);
        $prepare->execute();
        $result = $prepare->get_result();
        $prepare->close();
        return $result->fetch_assoc();
    }
    /**
     * @param int $userId<p>
     *      Id of searched user
     * </p>
     * @return array|null<p>
     *      Returns associative array with user data on successful, false otherwise
     * </p>
     */
    public function getUserCard(int $userId){
        $conn = new mysqli(Config::$dbHost,Config::$dbUser,Config::$dbPasswd, Config::$dbName);
        $prepare = $conn->prepare("select * from `users` where `user_id`=?");
        $prepare->bind_param('i',$userId);
        $prepare->execute();
        $result = $prepare->get_result();
        $prepare->close();
        return $result->fetch_assoc();
    }
    /**
     * @param int $userId<p>
     *      Id of searched user
     * </p>
     * @return array|null<p>
     *      Returns associative array with user id who have admin status on successful, false otherwise
     * </p>
     */
    public function checkAdminStatus(int $userId){
        $conn = new mysqli(Config::$dbHost,Config::$dbUser,Config::$dbPasswd, Config::$dbName);
        $prepare = $conn->prepare("select * from `admins` where `user_id`=?");
        $prepare->bind_param('i',$userId);
        $prepare->execute();
        $result = $prepare->get_result();
        $prepare->close();
        return $result->fetch_assoc();
    }
    /**
     * <p>Creates new user entry</p>
     * @param string $login
     * @param string $passwd
     * @param string $name
     * @param string $surname
     * @param string $gender
     * @param string $birthDate
     * @return array<p>
     *      Returns mysql result code
     * </p>
     */
    public function addUser(string $login, string $passwd, string $name, string $surname, string $gender, string $birthDate){
        $conn = new mysqli(Config::$dbHost,Config::$dbUser,Config::$dbPasswd, Config::$dbName);
        $prepare = $conn->prepare("insert into `users` set `login`=?, `passwd`=?, `name`=?, `surname`=?, `gender`=?, `birth_date`=?");
        $prepare->bind_param('ssssss',$login,$passwd,$name,$surname,$gender,$birthDate);
        $prepare->execute();
        $conn->close();
        $output = ['result_code'=>$prepare->errno.':'.$prepare->error];
        $prepare->close();
        return $output;

    }
    /**
     * <p>Updates data of chosen user</p>
     * @param int $userId
     * @param string $login
     * @param string $passwd
     * @param string $name
     * @param string $surname
     * @param string $gender
     * @param string $birthDate
     * @return array<p>
     *      Returns mysql result code
     * </p>
     */
    public function editUser(int $userId, string $login, string $passwd, string $name, string $surname, string $gender, string $birthDate){
        $conn = new mysqli(Config::$dbHost,Config::$dbUser,Config::$dbPasswd, Config::$dbName);
        $prepare = $conn->prepare("update `users` set `login`=?, `passwd`=?, `name`=?, `surname`=?, `gender`=?, `birth_date`=? where `user_id`=?");
        $prepare->bind_param('ssssssi',$login,$passwd,$name,$surname,$gender,$birthDate,$userId);
        $prepare->execute();
        $conn->close();
        $output = ['result_code'=>$prepare->errno.':'.$prepare->error];
        $prepare->close();
        return $output;
    }
    /**
     * @param int $userId<p>User id who needs to remove</p>
     * @return array<p>
     *      Returns mysql result code
     * </p>
     */
    public function removeUser(int $userId){
        $conn = new mysqli(Config::$dbHost,Config::$dbUser,Config::$dbPasswd, Config::$dbName);
        $prepare = $conn->prepare("delete from `users` where `user_id`=?");
        $prepare->bind_param('i',$userId);
        $prepare->execute();
        $conn->close();
        $output = ['result_code'=>$prepare->errno.':'.$prepare->error];
        $prepare->close();
        return $output;
    }
}
