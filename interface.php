<?php

require_once 'core/Controller.php';

session_start();
if(!isset($_COOKIE['sortOrder'],$_COOKIE['amountOnPage'],$_COOKIE['sortBy'])){
    setcookie('amountOnPage',Config::DEFAULT_AMOUNT_ON_PAGE);
    setcookie('sortBy',Config::DEFAULT_SORT_BY);
    setcookie('sortOrder',Config::DEFAULT_SORT_ORDER);
}

$controller = new Controller();

if(isset($_COOKIE['presence'])){
    switch($_POST['action']){
        case 'showAddForm':
            $page = $controller->showAddEditForm($controller::ADD_MODE);
            break;
        case 'showEditForm':
            $page = $controller->showAddEditForm($controller::EDIT_MODE);
            break;
        case 'createUser':
            $controller->addUser($_POST['login'],$_POST['passwd'],$_POST['name'],$_POST['surname'],$_POST['gender'],$_POST['birth_date']);
            $_GET['action']='toInterfacePage';
            break;
        case 'findUser':
            $page = $controller->showFoundUser($_POST['searchByLogin']);
            break;
        case 'editUser':
            $controller->editUser($_POST['user_id'],$_POST['login'],$_POST['passwd'],$_POST['name'],$_POST['surname'],$_POST['gender'],$_POST['birth_date']);
            $_GET['action']='toInterfacePage';
            break;
        case 'deleteUser':
            $controller->removeUser($_POST['user_id']);
            $_GET['action']='toInterfacePage';
            break;
        case 'sort':
            setcookie('amountOnPage',$_POST['amountOnPage']);
            setcookie('sortBy',$_POST['sortBy']);
            setcookie('sortOrder',$_POST['sortOrder']);
            $page = $controller->showUsersList(1,$_POST['amountOnPage'],$_POST['sortBy'],$_POST['sortOrder']);
            break;
    }
    switch($_GET['action']){
        case 'toPage':
            $page = $controller->showUsersList($_GET['pageNumber'],$_COOKIE['amountOnPage'],$_COOKIE['sortBy'],$_COOKIE['sortOrder']);
            break;
        case 'toInterfacePage':
            if(isset($_COOKIE['sortOrder'],$_COOKIE['amountOnPage'],$_COOKIE['sortBy'])){
                $page = $controller->showUsersList(1,$_COOKIE['amountOnPage'],$_COOKIE['sortBy'],$_COOKIE['sortOrder']);
            } else {
                $page = $controller->showUsersList(1,Config::DEFAULT_AMOUNT_ON_PAGE,Config::DEFAULT_SORT_BY,Config::DEFAULT_SORT_ORDER);
            }
            break;
    }
    if(!isset($page) || $page==''){
        $page = $controller->showUsersList(1,$_COOKIE['amountOnPage'],$_COOKIE['sortBy'],$_COOKIE['sortOrder']);
    }
    echo $page;
} else {
    header("Location: /index.php");
}
if(isset($_SESSION['messageCode'])){
    echo $controller->showMessageWindow($_SESSION);
    unset($_SESSION['messageCode']);
    unset($_SESSION['messageText']);
}