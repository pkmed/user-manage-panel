-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3307
-- Время создания: Май 26 2019 г., 18:55
-- Версия сервера: 10.2.7-MariaDB
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testjob`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `admins`
--

INSERT INTO `admins` (`admin_id`, `user_id`) VALUES
(2, 16);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `login` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `passwd` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('m','f') COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `login`, `passwd`, `name`, `surname`, `gender`, `birth_date`) VALUES
(16, 'dummy', '83328c06cc667e17d308701de6ac2ed4ae7659199c324ff333137084adb1bf981c780d3021bfdc2b3a8d0f04b94d559ac9129e65923a0ef5ecf5959dd158f12e', 'john', 'doe', 'm', '1973-05-06'),
(17, 'static', '75a6aac7bae8bf1e36030c054d93e7075bb9847dfaddc57f64a95b1a82b382bb3e9e9ee102ae02af5e877e8f45348b8f1dbdad0df5fcf30fc3a3c35205dc7c8e', 'wayne', 'static', 'm', '1965-11-04'),
(18, 'beard', 'de7c919c9182da3230c568b88a8a50cf5c4ea046451de1eb87434d713b48823b2d6cae03a7d05c5230cd48659022b21806e1bc1c8019076b92f2b43754a1d263', 'tony', 'campos', 'm', '1973-03-08'),
(19, 'kjdr', '8c23f1efe6a30e304389f1de17e721257120cf15babdfbbed90a435c0ac0196176133693466e907e5c9cb02d2531b72bd9f9e41d18ba386d3e8e59676dd1ff12', 'ken', 'jay', 'm', '1971-06-10'),
(20, 'fuchi', '419be8018ec0244130342281228b95a38f6ad553fe438e57c0fe28934fc251eed6b77dc5c75da597ad5d5e7b9508a288cb412f0fc600c3f824875ff265aaa626', 'koichi', 'fukuda', 'm', '1975-06-01'),
(21, 'oshdr', '78629640db01642b4aeba520105b1fec8f2e04e2fc9beda5f1fe7120b9c5e03ec1f491357d30b1b13ee4640552892f4e8086ccadbd98596df3af06fc7885d429', 'nick', 'oshiro', 'm', '1978-07-29');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_login` (`login`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
