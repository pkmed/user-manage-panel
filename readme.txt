1)Place files at root folder of domain.

2)Set data for access to database in setup.php
$mysql* variables are used only for setup database.
$mysqlMainAdmin* variables are setting authentication data for mysql user with all privileges to database.
$mysqlSiteAdmin* variables are used for database operations from an account on the site with administrator rights. If you change any of these fields, you must change the values of the corresponding fields in Config.php.

3)Run setup.php with web browser using "http://*your-domain*/setup.php".

Account with admin rights will be avaliable  by default by login: dummy and password: Kbd9JmpL.
For connect to mysql to manage database `testjob` you can use your accounts of root or user with all privileges alongside $mysqlMainAdminLogin.

After running setup.php you can delete it.