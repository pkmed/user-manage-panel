<?php

require_once 'core/Controller.php';

session_start();

$controller = new Controller();

$authForm = file_get_contents("templates/authForm.html");
$registerForm = $controller->showAddEditForm(Controller::REGISTER_MODE);

if(!isset($_COOKIE['presence']) && !isset($_POST['action'])){
    //in case if cookie is expired and page were requested
    unset($_SESSION['adminStatus']);
    unset($page);
    echo $authForm;
} else {
    switch($_POST['action']){
        case 'log-in':
            if($controller->loginUser($_POST['login'],$_POST['passwd'])){
                setcookie('presence',true,time()+3600);
                $page = $controller->showIdlePage();
            } else {
                $page = $authForm;
            }
            break;
        case 'register':
            if($controller->addUser($_POST['login'],$_POST['passwd'],$_POST['name'],$_POST['surname'],$_POST['gender'],$_POST['birth_date'])){
                $page = $authForm;
                $_SESSION['messageCode']='0';
                $_SESSION['messageText']='successful';
            } else {
                $page = $registerForm;
            }
            break;
        case 'toRegister':
            $page = $registerForm;
            break;
        case 'toPage':
            $page = $authForm;
            break;
    }
    unset($_POST);
    if(!isset($page)){
        //in case of request in one hour after page was left
        if(isset($_COOKIE['presence']) && $_SESSION['adminStatus']) {
            header("Location: /interface.php");
        } else {
            $page = $controller->showIdlePage();
        }
    }
}
echo $page;
if(isset($_SESSION['messageCode'])){
    echo $controller->showMessageWindow($_SESSION);
    unset($_SESSION['messageCode']);
    unset($_SESSION['messageText']);
}
