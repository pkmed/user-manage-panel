<?php

require_once 'core/Controller.php';

$mysqlDBHost = 'localhost';
$mysqlLogin = 'root';
$mysqlPassword = '';

$mysqlMainAdminLogin = 'testjobadmin';
$mysqlMainAdminPassword = 'qwerty';

$mysqlSiteAdminLogin = 'dummyAdmin';
$mysqlSiteAdminPassword = '12345';

$conn = new mysqli($mysqlDBHost,$mysqlLogin,$mysqlPassword);

$conn->multi_query(
	"CREATE DATABASE IF NOT EXISTS `testjob` COLLATE=utf8_unicode_ci;".
	"USE `testjob`;".
	file_get_contents("dump.sql").
    "CREATE USER '{$mysqlMainAdminLogin}'@'localhost' IDENTIFIED BY '{$mysqlMainAdminPassword}';".
    "GRANT ALL PRIVILEGES ON testjob.* TO '{$mysqlMainAdminLogin}'@'localhost';".
    "CREATE USER '{$mysqlSiteAdminLogin}'@'localhost' IDENTIFIED BY '{$mysqlSiteAdminPassword}';".
	"grant select,insert,update,delete on testjob.users to '{$mysqlSiteAdminLogin}'@'localhost';".
	"grant select on testjob.admins to '{$mysqlSiteAdminLogin}'@'localhost';".
	"flush privileges;"
);
$conn->close();