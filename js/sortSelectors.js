function get_cookie ( cookie_name )
{
    var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

    if ( results )
        return ( unescape ( results[2] ) );
    else
        return null;
}
function setOrderBy(){
    var orderBy = get_cookie("sortBy");
    var options = Array.prototype.slice.call(document.getElementById("sortBySelect").options);
    options.forEach(function(item){
        if(item.getAttribute("value")==orderBy){
            item.setAttribute("selected",'');
        } else {
            item.removeAttribute("selected");
        }
    });
}
function setAmountOnPage(){
    var amountOnPage = get_cookie("amountOnPage");
    var options = Array.prototype.slice.call(document.getElementById("amountOnPageSelect").options);
    options.forEach(function(item){
        if(item.getAttribute("value")==amountOnPage){
            item.setAttribute("selected",'');
        } else {
            item.removeAttribute("selected");
        }
    });
}
function setSortOrder(){
    var sortOrder = get_cookie("sortOrder");
    var options = Array.prototype.slice.call(document.getElementById("sortOrderSelect").options);
    options.forEach(function(item){
        if(item.getAttribute("value")==sortOrder){
            item.setAttribute("selected",'');
        } else {
            item.removeAttribute("selected");
        }
    });
}
function setSortSelectors(){
    setOrderBy();
    setAmountOnPage();
    setSortOrder();
}