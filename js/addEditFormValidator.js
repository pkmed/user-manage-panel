function validateForm(){
    var loginField = document.getElementById("login");
    var passwdField = document.getElementById("passwd");
    var nameField = document.getElementById("name");
    var surnameField = document.getElementById("surname");
    var genderField = document.getElementById("gender");
    var birth_dateField = document.getElementById("birth_date");

    var login = loginField.value=='' ? loginField.placeholder : loginField.value;
    var passwd = passwdField.value=='' ? passwdField.placeholder : passwdField.value;
    var name = nameField.value=='' ? nameField.placeholder : nameField.value;
    var surname = surnameField.value=='' ? surnameField.placeholder : surnameField.value;
    var gender = genderField.value=='' ? genderField.placeholder : genderField.value;
    var birth_date = birth_dateField.value=='' ? birth_dateField.placeholder : birth_dateField.value;



    if(login =='' || passwd =='' || name=='' || surname=='' || gender=='' || !birth_date.match("[0-9][^\\-]*")){
        alert('Form contains empty fields');
        return false;
    } else if(name.match("/\\d|\\W*/i") || surname.match("/\\d|\\W*/i")){
        alert('Name and surname must not contain numbers');
        return false;
    } else if(gender.match("/(m|f){1}/i")){
        alert('gender must be specified by letter m or f case insensitive');
        return false;
    } else if(!birth_date.match("[0-9]{4}-[0-9]{2}-[0-9]{2}")){
        alert('Birth date must be entered in the format yyyy-mm-dd');
        return false;
    } else {
        loginField.value = login;
        passwdField.value = passwd;
        nameField.value = name;
        surnameField.value = surname;
        genderField.value = gender;
        birth_dateField.value = birth_date;
        return true;
    }
}